<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClientRequest;
use App\Service\DeleteClientService;
use App\Service\ShowClientService;
use App\Service\StoreClientService;
use App\Service\UpdateClientService;

class ClientsController extends Controller
{
    public function index(ShowClientService $service)
    {
        return $service->start('getAllClients');
    }

    public function show($id, ShowClientService $service)
    {
        return $service->getClient($id);
    }

    public function update(StoreClientRequest $request, int $id, UpdateClientService $service)
    {
        $service->start($request->all(), $request->input('method'), $id);
    }

    public function store(StoreClientRequest $request, StoreClientService $service)
    {
        $service->start($request->all(), $request->input('method'));
    }

    public function destroy($id, DeleteClientService $service)
    {
        $service->deleteClient($id);
    }

}
