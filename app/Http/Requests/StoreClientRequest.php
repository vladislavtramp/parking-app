<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request->all();

        $arr = [
            'client' => ['sometimes', 'array'],
            'client.name' => ['sometimes', 'min:3'],
            'client.gender' => ['sometimes', 'min: 1'],
            'client.phone' => ['sometimes', 'min: 3', 'unique:App\Models\Client,phone'],
            'client.address' => ['sometimes', 'min: 1'],
            'client.cars' => ['sometimes', 'min: 1'],

            'cars' => ['sometimes', 'array'],
            'cars.*.id' => ['sometimes'],
            'cars.*.manufacturer' => ['sometimes', 'min: 1'],
            'cars.*.model' => ['sometimes', 'min: 1'],
            'cars.*.color' => ['sometimes', 'min: 1'],
            'cars.*.number' => ['sometimes', 'min: 1', 'unique:App\Models\Car,number'],

            'updateCars' => ['sometimes', 'array'],
            'updateCars.*.id' => ['sometimes'],
            'updateCars.*.manufacturer' => ['sometimes', 'min: 1'],
            'updateCars.*.model' => ['sometimes', 'min: 1'],
            'updateCars.*.color' => ['sometimes', 'min: 1'],
            'updateCars.*.number' => ['sometimes', 'min: 1'],

            'newCars' => ['sometimes', 'array'],
            'newCars.*.id' => ['sometimes'],
            'newCars.*.manufacturer' => ['sometimes', 'min: 1'],
            'newCars.*.model' => ['sometimes', 'min: 1'],
            'newCars.*.color' => ['sometimes', 'min: 1'],
            'newCars.*.number' => ['sometimes', 'min: 1', 'unique:App\Models\Car,number'],
        ];

        if ($request['method'] === 'updateClient'){
            $id = $this->route()->parameters['client'];

            $phone = DB::select('SELECT phone FROM clients WHERE id = :id', [':id' => $id]);

            if ($request['client']['phone'] === $phone[0]->phone){
                $arr['client.phone'][2] = '';
            }
        }

        return $arr;

    }

    public function messages()
    {
        return [

            'client.name.min' => 'Имя должно содержать от 3 символов',
            'client.gender.min' => 'Необходимо указать пол',
            'client.cars.min' => 'Нужно заполнить минимум 1 автомобиль',
            'client.phone.unique' => 'Такой номер уже существует',
            'client.phone.min' => 'Обязательное поле',
            'client.address.min' => 'Обязательное поле',

            'cars.*.manufacturer.min' => 'Обязательное поле',
            'cars.*.model.min' => 'Обязательное поле',
            'cars.*.color.min' => 'Обязательное поле',
            'cars.*.number.unique' => 'Такой номер уже существует',
            'cars.*.number.min' => 'Обязательное поле',

            'newCars.*.manufacturer.min' => 'Обязательное поле',
            'newCars.*.model.min' => 'Обязательное поле',
            'newCars.*.color.min' => 'Обязательное поле',
            'newCars.*.number.unique' => 'Такой номер уже существует',
            'newCars.*.number.min' => 'Обязательное поле',

        ];

    }

}
