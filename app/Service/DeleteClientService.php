<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class DeleteClientService
{
    public function deleteCar($id)
    {
        $client = DB::select('SELECT client_id FROM cars WHERE id = :id', [':id' => $id]);
        DB::select('DELETE FROM cars WHERE id = :id', [':id' => $id]);
        DB::select('UPDATE clients SET cars = (SELECT count(cars.client_id) FROM cars WHERE cars.client_id = :client)WHERE id = :id', [':client' => $client[0]->client_id, ':id' => $client[0]->client_id]);
    }

    public function deleteClient($id)
    {
        DB::select('DELETE FROM cars WHERE client_id = :id', [':id' => $id]);
        DB::select('DELETE FROM clients WHERE id = :id', [':id' => $id]);
    }
}
