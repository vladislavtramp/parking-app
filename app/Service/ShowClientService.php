<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class ShowClientService
{
    public function start(string $method, $id = null)
    {
        if (method_exists($this, $method)){
            if (is_int($id)){
                return $this->$method($id);
            } else{
                return $this->$method();
            }
        }
    }

    public function getAllClients()
    {
        $clients = DB::table('clients')
            ->select('id', 'name', 'phone', 'cars')
            ->paginate(6);

        return json_encode($clients);
    }

    public function getAllCarFromParking()
    {
        $cars = DB::table('clients')
            ->join('cars', 'clients.id', '=', 'cars.client_id')
            ->select('clients.id', 'clients.name', 'cars.model', 'cars.number')
            ->where('cars.status', '=', 1)
            ->paginate(6);

        return json_encode($cars);
    }

    public function getClient($id)
    {
        $client = DB::select('SELECT clients.name, clients.gender, clients.phone, clients.address, cars.manufacturer, cars.model, cars.color, cars.number, cars.id
FROM clients INNER JOIN cars ON clients.id = cars.client_id WHERE client_id = :id', [':id' => $id]);

        return json_encode($client);
    }

    public function getClientsList(): array
    {
        return DB::select('SELECT clients.name, clients.id FROM clients');
    }

    public function getCarsClientsList($id)
    {
        $cars = DB::select('SELECT cars.model, cars.id, cars.model, clients.name, cars.number FROM cars INNER JOIN clients on cars.client_id = clients.id WHERE cars.client_id = :id AND cars.status = 0', [':id' => $id]);

        return json_encode($cars);
    }
}
