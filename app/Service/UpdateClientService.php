<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class UpdateClientService
{
    public function start(array $request, $method, $id = 0)
    {
        if (method_exists($this, $method)) {
            if ($request['method'] == 'updateClient' || $request['method'] == 'updateCars') {
                $this->$method($request, $id);
            } else {
                $this->$method($request);
            }
        }
    }

    public function updateClient(array $client, int $id)
    {
        $name = $client['client']['name'];
        $gender = $client['client']['gender'];
        $address = $client['client']['address'];
        $phone = $client['client']['phone'];

        DB::select('UPDATE clients SET name = :name, gender = :gender, phone = :phone, address = :address WHERE id = :id', [
            ':name' => $name, ':gender' => $gender, ':phone' => $phone,
            ':address' => $address, ':id' => $id
        ]);
    }

    public function updateCars(array $aboutCar)
    {
        $aboutCar = $aboutCar['updateCars'];

        foreach ($aboutCar as $car) {
            DB::select('UPDATE cars SET manufacturer = :manufacturer, model = :model, color = :color, number = :number WHERE id = :id', [
                ':manufacturer' => $car['manufacturer'],
                ':model' => $car['model'],
                ':color' => $car['color'],
                ':number' => $car['number'],
                ':id' => $car['id']
            ]);
        }
        $client = DB::select('SELECT client_id FROM cars WHERE id = :id', [
            ':id' => $aboutCar[0]['id']
        ]);

        $this->updateClientCars($client[0]->client_id);
    }

    public function updateStatus(array $car)
    {
        DB::select('UPDATE cars SET status = 0 WHERE number = :id', [
            ':id' => $car['number']
        ]);
    }

    public function updateClientCars($id)
    {
        DB::select('UPDATE clients SET cars = (SELECT count(cars.client_id) FROM cars WHERE cars.client_id = :client)WHERE id = :id', [
            ':client' => $id, ':id' => $id]);
    }

    public function updateCarStatus($id)
    {
        DB::select('UPDATE cars SET status = 1 WHERE id = :id', [
            ':id' => $id['id']
        ]);
    }

}
