<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class StoreClientService
{
    public function start(array $request, string $method)
    {
        if (method_exists($this, $method)) {
            if (isset($request['newCars'])) {
                $this->$method($request, $request['newCars'][0]['clientId']);
            }
            $this->$method($request);
        }
    }

    public function storeClient($client)
    {
        $name = $client['client']['name'];
        $gender = $client['client']['gender'];
        $address = $client['client']['address'];
        $cars = $client['client']['cars'];
        $phone = $client['client']['phone'];

        DB::select('INSERT INTO clients(name, gender, phone, address, cars) VALUES(:name, :gender, :phone, :address, :cars)', [
            ':name' => $name,
            ':gender' => $gender,
            ':phone' => $phone,
            ':address' => $address,
            ':cars' => $cars]);

        $this->storeCars($client['cars'], $phone);
    }

    public function storeCars(array $aboutCar, int $phone)
    {
        if (isset($aboutCar['newCars'])) {
            $aboutCar = $aboutCar['newCars'];
            $clientId = $phone;
        } else {
            $fetchClient = $this->fetchClientByPhone($phone);
            $clientId = $fetchClient[0]->id;
        }

        foreach ($aboutCar as $car) {
            DB::select('INSERT INTO cars(client_id, manufacturer, model, color, number)
        VALUES(:client_id, :manufacturer, :model, :color, :number)', [
                ':client_id' => $clientId,
                ':manufacturer' => $car['manufacturer'],
                ':model' => $car['model'],
                ':color' => $car['color'],
                ':number' => $car['number']]);
        }

        (new UpdateClientService())->updateClientCars($clientId);
    }

    protected function fetchClientByPhone(int $phone): array
    {
        return DB::select('SELECT id FROM clients WHERE phone = :phone', [':phone' => $phone]);
    }

}
