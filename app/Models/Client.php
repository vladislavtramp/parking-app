<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'gender', 'phone', 'address', 'cars'];

    public function car()
    {
        return $this->hasMany(Car::class);
    }
}
