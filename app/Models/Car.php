<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = ['client_id', 'manufacturer', 'model', 'color', 'number', 'status'];

    public function client()
    {
        return $this->hasOne(Client::class);
    }
}
