<?php

namespace App\Providers;

use App\Service\StoreClientService;
use Illuminate\Support\ServiceProvider;

class StoreClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StoreClientService::class, function ($spp){
            return new StoreClientService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
