<?php

namespace App\Providers;

use App\Service\ShowClientService;
use Illuminate\Support\ServiceProvider;

class ShowClientsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ShowClientService::class, function($app){
            return new ShowClientService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


}
