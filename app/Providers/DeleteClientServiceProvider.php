<?php

namespace App\Providers;

use App\Service\DeleteClientService;
use Illuminate\Support\ServiceProvider;

class DeleteClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DeleteClientService::class, function($app){
            return new DeleteClientService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
