<?php

namespace App\Providers;

use App\Service\UpdateClientService;
use Illuminate\Support\ServiceProvider;

class UpdateClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UpdateClientService::class, function($app){
            return new UpdateClientService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
