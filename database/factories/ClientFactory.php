<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Client::class;

    public function definition()
    {
        return [
            'name' => $this->faker->firstName,
            'gender' => 'Мужской',
            'phone' => $this->faker->numberBetween(10, 2500),
            'address' => $this->faker->address,
            'cars' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
