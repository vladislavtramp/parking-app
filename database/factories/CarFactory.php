<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Car::class;

    public function definition()
    {
        return [
            'model' => $this->faker->lastName,
            'manufacturer' => $this->faker->firstName,
            'client_id' => Client::get()->random()->id,
            'color' => $this->faker->colorName,
            'number' => $this->faker->numberBetween(5, 2000),
            'status' => $this->faker->numberBetween(0, 1),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
