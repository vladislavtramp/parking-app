<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\Client;
use App\Service\UpdateClientService;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $clients = Client::factory(15)->create();

        Car::factory(30)->create();

        foreach ($clients as $client){
            (new UpdateClientService())->updateClientCars($client['id']);
        }
    }
}
