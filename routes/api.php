<?php

use App\Http\Controllers\Api\v1\CarsController;
use App\Http\Controllers\Api\v1\ClientsController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('parking')->group(function () {
    Route::get('/clients', [ClientsController::class, 'index']);
    Route::get('/clients/{client}', [ClientsController::class, 'show']);
    Route::put('/clients/{client}', [ClientsController::class, 'update']);
    Route::post('/clients', [ClientsController::class, 'store']);
    Route::delete('/clients/{client} ', [ClientsController::class, 'destroy']);

    Route::get('/cars', [CarsController::class, 'index']);
    Route::get('/cars/{car}', [CarsController::class, 'show']);
    Route::put('/cars/{car}', [CarsController::class, 'update']);
    Route::post('/cars', [CarsController::class, 'store']);
    Route::delete('/cars/{car} ', [CarsController::class, 'destroy']);
});
