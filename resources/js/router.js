import VueRouter from "vue-router";

import Clients from './components/Clients'
import AddClient from "./components/AddClient";
import UpdateClient from "./components/UpdateClient";
import Cars from "./components/Cars";

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/clients',
            component: Clients
        },

        {
            path: '/',
            component: Cars
        },

        {
            path: '/add',
            component: AddClient
        },

        {
            path: '/edit/:id',
            component: UpdateClient
        },
    ]
})
