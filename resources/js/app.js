import Vue from "vue";
import router from "./router";
import VueRouter from "vue-router";
import VeeValidate from 'vee-validate';
import VeeValidateLaravel from 'vee-validate-laravel';

Vue.use(VeeValidate);
Vue.use(VeeValidateLaravel);

require('./bootstrap');

window.Vue = require('vue').default;

Vue.use(VueRouter);

Vue.component('layout-navbar', require('./components/layouts/Navbar.vue').default);
Vue.component('layout-left-menu', require('./components/layouts/LeftMenu.vue').default);

Vue.component('all-clients', require('./components/Clients.vue').default);
Vue.component('parking', require('./components/Cars').default);
Vue.component('add-client', require('./components/AddClient.vue').default);
Vue.component('update-client', require('./components/UpdateClient.vue').default);


const app = new Vue({
    el: '#app',
    router,
});
