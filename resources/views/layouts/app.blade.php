<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('./css/main.css') }}">
</head>
<body>
    <div id="app">
        <div class="d-flex">
            <layout-left-menu></layout-left-menu>
            <div class="">
                <layout-navbar></layout-navbar>
                <div class="bg d-flex align-items-lg-center justify-content-center">
                    <div class="">
                        <router-view></router-view>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
